# Introduction to monitoring using Prometheus and Grafana 
---

## Introduction to application monitoring
Monitoring an application's health and metrics helps us manage it better, notice unoptimized behavior and get closer to its performance. This especially holds true when we're developing a system with many microservices, where monitoring each service can prove to be crucial when it comes to maintaining our system.

Based on this real-time monitoring information, we can draw conclusions and decide which microservice needs to scale if further performance improvements can't be achieved with the current setup.

## What is Prometheus ? What is Grafana ?
- **Prometheus** is an open-source monitoring system that was originally built by SoundCloud. It consists of the following core components -

   - A _data scraper_ that pulls metrics data over HTTP periodically at a configured interval.
   - A _time-series_ database to store all the metrics data.
   - A _simple user interface_ where you can visualize, query, and monitor all the metrics.

- **Grafana**
Grafana allows you to bring data from various data sources like Elasticsearch, Prometheus, Graphite, InfluxDB etc, and visualize them with beautiful graphs.
It also lets you set alert rules based on your metrics data. When an alert changes state, it can notify you over email, slack, or various other channels.
 Prometheus dashboard also has simple graphs. But Grafana’s graphs are way better. 

 ## Setting Up a simple Spring Boot Application that exposes metrics

 We develop here a simple Spring Boot Microservice application that provides a REST Endpoint `/hello` that runs on the default port `8080`. This application also has the `spring-boot-starter-actuator` dependency, which provides production-ready endpoints that you can use for your application. These endpoints fall under a common prefix of `/actuator` and are, by default, protected.

Expose them individually, or all at once, by adding the following properties in application.properties:

```shell
management.endpoints.web.exposure.include=*
```
To check, let's navigate our browser to http://localhost:8080/actuator:

![actuator](images/actuator.png)

You can see all the endpoints that Actuator exposes such as `/health`, `/metrics`, `/mappings`, etc. Let's open up the /metrics endpoint of the Actuator by navigating our browser to `http://localhost:8080/actuator/metrics`:
![actuator](images/metrics.png)
As you can see, there's a bunch of information about our application here, such as information about threads, Tomcat sessions, classes, the buffer, etc. Let's go deeper and retrieve information about the JVM memory used:
![actuator](images/jvmused.png)
Now, using the Spring Boot Actuator like this does yield a lot of information about your application, but it's not very user-friendly.
 Tools like **Prometheus**, **Netflix Atlas**, and **Grafana** are more commonly used for the monitoring and visualization and are language/framework-independent.

Each of these tools has its own set of data formats and converting the `/metrics` data for each one would be a pain. To avoid converting them ourselves, we need a vendor-neutral data provider, such as **Micrometer**.
> **Micrometer**
To solve this problem of being a vendor-neutral data provider, Micrometer came to be. It exposes Actuator metrics to external monitoring systems such as Prometheus, Netflix Atlas, AWS Cloudwatch, and many more. 

Micrometer automatically exposes /actuator/metrics data into something your monitoring system can understand. All you need to do is include that vendor-specific micrometer dependency in your application.
Micrometer is a separate open-sourced project and is not in the Spring ecosystem, so we have to explicitly add it as a dependency. Since we will be using Prometheus, let's add it's specific dependency in our pom.xml:

```xml
<dependency>
    <groupId>io.micrometer</groupId>
    <artifactId>micrometer-registry-prometheus</artifactId>
</dependency>
```
The prometheus endpoint exposes metrics data in a format that can be scraped by a Prometheus server. You can see the exposed metrics data by navigating to the prometheus endpoint.

![prom](images/prometheusendpoint.png)


## Setting Up Prometheus

Prometheus is a time-series database that stores our metric data by pulling it (using a built-in data scraper) periodically over HTTP. The intervals between pulls can be configured, of course, and we have to provide the URL to pull from. It also has a simple user interface where we can visualize/query on all of the collected metrics.

Let's configure Prometheus, and more precisely the scrape interval, the targets, etc. To do that, we'll be using the `prometheus.yml` file:

```yaml
global:
  scrape_interval: 10s

scrape_configs:
  - job_name: 'spring_micrometer'
    metrics_path: '/actuator/prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['YOUR-HOST-IP:8080']

```
As you can see, we have a scrape_configs root key where we can define a list of jobs and specify the URL, metrics path, and the interval. If you'd like to read more about Prometheus configurations, feel free to visit the official documentation.
> Note: Please make sure to replace the **YOUR-HOST_IP** with the IP address of the machine where your Spring Boot application is running. Note that, localhost won’t work here because we’ll be connecting to the HOST machine from the docker container. You must specify the network IP address.
Now, we can run Prometheus using the Docker command:

```shell
$ docker run -d -p 9090:9090 -v <path-to-prometheus.yml>:/etc/prometheus/prometheus.yml prom/prometheus
```
To see Prometheus dashboard, navigate your browser to `http://localhost:9090`:
![dash](images/promdashboard.png)

To check if Prometheus is actually listening to the Spring app, you can go to the `/targets` endpoint:
![dash](images/targets.png)

Let's go back to the home page and select a metric from the list and click Execute:
![example](images/promexample.png)

####  Prometheus Query Language - PromQL
Another thing to note is - Prometheus has its own query language called PromQL. It allows the user to select and aggregate time series data in real-time, storing it either in graph or tabular format. Alternatively, you can feed it to an external API through HTTP.

If you'd like to read more about PromQL, the [official documentation](https://prometheus.io/docs/prometheus/latest/querying/basics/) covers it quite nicely.

## Using Grafana
While Prometheus does provide some crude visualization, Grafana offers a rich UI where you can build up custom graphs quickly and create a dashboard out of many graphs in no time. You can also import many community built dashboards for free and get going.

Grafana can pull data from various data sources like Prometheus, Elasticsearch, InfluxDB, etc. It also allows you to set rule-based alerts, which then can notify you over Slack, Email, Hipchat, and similar.

```shell
$ docker run -d -p 3000:3000 grafana/grafana
```
If you visit `http://localhost:3000`, you will be redirected to a login page:
![example](images/grafanadash.png)
The default username is admin and the default password is admin. You can change these in the next step, which is highly recommended:
![example](images/grafanaWelcome.png)
Since Grafana works with many data sources, we need to define which one we're relying on. Select Prometheus as your data source:
![example](images/grafanadatasource.png)
Now, add the URL that Prometheus is running on, in our case `http://localhost:9090` and select Access to be through a browser.

At this point, we can save and test to see if the data source is working correctly:
![example](images/grafanacreatedatasource.png)
As previously said, Grafana has a ton of pre-built dashboards. For Spring Boot projects, the [JVM dashboard is popular](https://grafana.com/grafana/dashboards/4701):


Input the URL for the dashboard, select "Already created Prometheus datasource" and then click Import:
![grafana](images/grafanafinaldasboard.png)