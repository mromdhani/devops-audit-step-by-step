#Monitoring Kubernetes Clusters
---
## Introduction to Kubernetes monitoring
 Kubernetes has rapidly taken over the container ecosystem to become the true leader of container orchestration platforms. Despite Kubernetes’s popularity, monitoring Kubernetes presents many challenges. To understand these challenges and their solutions, it’s best to start with a review of how Kubernetes works.

#### Understanding Kubernetes and Its Abstractions
At the **infrastructure** level, a Kubernetes cluster is a set of physical or virtual machines acting in a specific role. The machines acting in the role of Master act as the brain of all operations and are charged with orchestrating containers that run on all of the Nodes.
- **Master components manage the life cycle of a pod**:

  - **apiserver**: main component exposing APIs for all the other master components
  - **scheduler**: uses information in the pod spec to decide on which node to run a pod
  - **controller-manager**: responsible for node management (detecting if a node fails), pod replication, and endpoint creation
  - **etcd**: key/value store used for storing all internal cluster data

- **Node components** are worker machines in Kubernetes, managed by the master. Each node contains the necessary components to run pods:

   - **kubelet**: handles all communication between the master and the node on which it is running. It interfaces with the container runtime to deploy and monitor containers
   - **kube-proxy**: is in charge with maintaining network rules for the node. It also handles communication between pods, nodes, and the outside world.
  - **container runtime**: runs containers on the node.

From a **logical** perspective, a Kubernetes deployment is comprised of various components, each serving a specific purpose within the cluster:

- **Pods**: are the basic unit of deployment within Kubernetes. A pod consists of one or more containers that share the same network namespace and IP address.
- **Services**: act like a load balancer. They provide an IP address in front of a pool (set of pods) and also a policy that controls access to them.
- **ReplicaSets**: are controlled by deployments and ensure that the desired number of pods for that deployment are running.
- **Namespaces**: define a logical segregation for different kind of resources like pods and/or services.
- **Metadata**: marks containers based on their deployment characteristics.
#### Monitoring Kubernetes
Monitoring an application is absolutely required if we want to anticipate problems and have visibility of potential bottlenecks in a dev or production deployment.
To help monitor the cluster and the many moving parts that form a deployment, Kubernetes ships with tools for monitoring the cluster:
- **Liveness and Readiness Probes**: actively monitor the health of a container. If the probe determines that a container is no longer healthy, the probe will restart it.
- **cAdvisor** is an open source agent that monitors resource usage and analyzes the performance of containers. Originally created by Google, cAdvisor is now integrated with the Kubelet. It collects, aggregates, processes and exports metrics such as CPU, memory, file and network usage for all containers running on a given node.
- **The kubernetes dashboard** is an add-on which gives an overview of the resources running on your cluster. It also gives a very basic means of deploying and interacting with those resources.
![dash](images/dashboard.png)

There are many additional solutions you can use to monitor Kubernetes like **Prometheus**, **Grafana**, **DataDog**, **Sysdig** ...

>**What are the differences between monitoring a Kubernetes Cluster and a traditional Virtual Machine?**
The use of Kubernetes in your infrastructure changes the way you will monitor it. When you don’t use Kubernetes (nor Swarm), you know exactly where every service of your application is deployed, and this doesn’t change. That is not the case when you use Kubernetes: you don’t know where your pods are because this is not static. Furthermore, you can scale your infrastructure whenever you want to and so the number of VM (Virtual Machine) you monitor will change: a classic monitoring tool can’t do that.

## How Prometheus compares to other Kubernetes monitoring tools
Prometheus released version 1.0 during 2016, so it’s a fairly recent technology. There were a wealth of tried-and-tested monitoring tools available when Prometheus first appeared. How does Prometheus compare with other veteran monitoring projects?

  - **Key-value** vs **dot-separated dimensions**: Several engines like StatsD/Graphite use an explicit dot-separated format to express dimensions, effectively generating a new metric per label. This method can become cumbersome when trying to expose highly dimensional data (containing lots of different labels per metric). Flexible query-based aggregation becomes more difficult as well.
  - **Event logging** vs **metrics recording**: InfluxDB / Kapacitor are more similar to the Prometheus stack. They use label-based dimensionality and the same data compression algorithms. Influx is, however, more suitable for event logging due to its nanosecond time resolution and ability to merge different event logs. Prometheus is more suitable for metrics collection and has a more powerful query language to inspect them.
  - **Blackbox** vs **whitebox** monitoring: As we mentioned before, tools like Nagios/Icinga/Sensu are suitable for host/network/service monitoring, classical sysadmin tasks. Nagios, for example, is host-based. If you want to get internal detail about the state of your micro-services (aka whitebox monitoring), Prometheus is a more appropriate tool.

## The challenges of microservices and Kubernetes monitoring with Prometheus
There are unique challenges unique to monitoring a Kubernetes cluster(s) that need to be solved for in order to deploy a reliable monitoring / alerting / graphing architecture.

- Monitoring containers: **visibility**
Containers are lightweight, mostly immutable black boxes, which can present monitoring challenges… The Kubernetes API and the kube-state-metrics (which natively uses prometheus metrics) solve part of this problem by exposing Kubernetes internal data such as number of desired / running replicas in a deployment, unschedulable nodes, etc.

- Prometheus is a good fit for **microservices** because you just need to expose a metrics port, and thus don’t need to add too much complexity or run additional services. Often, the service itself is already presenting a HTTP interface, and the developer just needs to add an additional path like /metrics. 
In some cases, the service is not prepared to serve Prometheus metrics and you can’t modify the code to support it. In that case, you need to deploy a Prometheus exporter bundled with the service, often as a sidecar container of the same pod.

- **Dynamic monitoring**: changing and volatile infrastructure
As we mentioned before, ephemeral entities that can start or stop reporting any time are a problem for classical, more static monitoring systems.
Prometheus has several autodiscover mechanisms to deal with this. The most relevant for this guide are:

    - _Consul_: A tool for service discovery and configuration. Consul is distributed, highly available, and extremely scalable.

    - _Kubernetes_: Kubernetes Service Discovery configurations allow retrieving scrape targets from Kubernetes’ REST API and always staying synchronized with the cluster state.

    - _Prometheus Operator_: To automatically generate monitoring target configurations based on familiar Kubernetes label queries. We will focus on this deployment option later on.

## Kubernetes monitoring with Prometheus: Architecture overview
This diagram covers the basic entities we want to deploy in our Kubernetes cluster:
![arch](images/prometheus_kubernetes_diagram_overview.png)
1. The Prometheus servers need as much target auto discovery as possible. There are several options to achieve this: Consul, Prometheus Kubernetes SD plugin, The Prometheus operator and its Custom Resource Definitions
2. Apart from application metrics, we want Prometheus to collect metrics related to the Kubernetes services, nodes and orchestration status.
    - Node exporter, for the classical host-related metrics: cpu, mem, network, etc.
    - Kube-state-metrics for orchestration and cluster level metrics: deployments, pod metrics, resource reservation, etc.
    - Kube-system metrics from internal components: kubelet, etcd, dns, scheduler, etc.
3. Prometheus can configure rules to trigger alerts using PromQL, alertmanager will be in charge of managing alert notification, grouping, inhibition, etc.
4. The alertmanager component configures the receivers, gateways to deliver alert notifications.
5. Grafana can pull metrics from any number of Prometheus servers and display panels and Dashboards.

## Deploy Prometheus + Grafana to Kubernetes by Helm 3
Let's install Prometheus and Grafana in our Minikube kubernetes cluster and start monitoring. We are using here a Helm 3 chart that installs a prometheus Operator.

**What is Prometheus Operator ?**
Operators were introduced by CoreOS as a class of software that operates other software, putting operational knowledge collected by humans into software. 
The [Prometheus Operator](https://github.com/coreos/prometheus-operator/blob/master/Documentation/user-guides/getting-started.md) serves to make running Prometheus on top of Kubernetes as easy as possible, while preserving Kubernetes-native configuration options. We are going to use the Prometheus Operator to: 
- Perform the initial installation and configuration of the full Kubernetes-Prometheus stack:
    - Prometheus servers
    - Alertmanager
    - Grafana
    - Host node_exporter
    - kube-state-metrics
- Define metric endpoint autoconfiguration using the ServiceMonitor entities
- Customize and scale the services using the Operator CRDs and ConfigMaps, making our configuration fully portable and declarative
The following is a components diagram of the Kubernetes monitoring system build by Prometheus:
![opera](images/prometheus_operator_diagram-1170x995.png)
### Step 1. Add repository of stable charts
Helm3 has not default repository.
```shell
helm repo add stable https://kubernetes-charts.storage.googleapis.com
```
### Step 2. Install prometheus-operator

```shell
helm install my-prometheus-operator stable/prometheus-operator
```

### Step 3. Show pods

```shell
kubectl --namespace default get pods -l "release=my-prometheus-operator"
```

### Step 3. Show Grafana UI

```shell
kubectl port-forward $(kubectl get pods --selector=app=grafana --output=jsonpath="{.items..metadata.name}") 3000
```
Open it `http://localhost:3000/` The password is `admin/prom-operator`
 
Try to monitor the CPU and Memory consumption of one of your Clustor pods.
![moni](images/containerdashbord.png)