# Introduction to Nagios
---

## What is Nagios ?
Nagios is an open source tool for **system monitoring**. It means that it watches servers or other devices on your network and makes sure that they are working properly. Nagios constantly checks if other **machines** are working properly. It also verifies that various **services** on those machines are working properly. In addition, Nagios can accept information from other processes or machines regarding their status; for example, a web server can send information to Nagios if it is overloaded.

The main purpose of system monitoring is to detect if any system is not working properly as soon as possible and notify the appropriate staff, and if possible, try to resolve the error—such as by restarting system services if needed.

System monitoring in Nagios is split into two categories of objects, hosts and services:

- **Hosts** represent a physical or virtual device on your network (servers, routers, workstations, printers, and so on)
- **Services** are particular functionalities, for example, a Secure Shell (SSH) server (sshd process on the machine) can be defined as a service to be monitored. Each service is associated with a host on which it is running. In addition, machines can be grouped into host groups.

A major benefit of Nagios’ performance checks is that it only uses four distinct states: **Ok**, **Warning**, **Critical**, and **Unknown**. Another advantage is that it is based on the framework of plugins, allowing you to reuse existing plugins or to develop your own plugin.

Nagios is available for al plaforms. It is downloadable from [here](https://www.nagios.org/downloads/). Nagios Core is the free version of Nagios and it serves as the basic event scheduler, event processor, and alert manager for elements that are monitored. Nagios XI is the commercial and extended version of Nagios Core.

## How Nagios works ?
Nagios relies on external programs called plugins to monitor machines connected to the network. Without these plugins, Nagios has no internal means to monitor equipment and collect information. Nagios is considered a task and event scheduler. Its operating principle is quite simple: at a period time, it runs plugins to collect information on the states of the machines. 

A plugin is generally a script (Shell, PHP, Perl, etc.) or a program and it can be executed locally in the Nagios server or remotely in client machines using agents. The return of the execution of a plugin is used by Nagios to find out the status of the controlled machine.  The configuration of Nagios is based on objects.  The configuration of Nagios is saved in flat files.
![architecture](images/nagios_architecture.png)
Nagios comes with a set of standard plugins that allow performance checks for almost all the services your company might use or offer. Moreover, if you need to perform a specific check (for example, connect to a web service and invoke methods), you can either add existing plugins or write your own plugins.

## Spinning Up Nagios

**Installing**
Let's pull the the Nagios Core Docker image from `jsonrivers`. It contains _Nagios Core 4.4.5_ running on _Ubuntu 16.04 LTS_ with NagiosGraph & NRPE.
```shell
$ docker pull jasonrivers/nagios:latest
```

**Running**
Run with the example configuration with the following:

```shell
$ docker run --name nagios4 -p 0.0.0.0:8080:80 jasonrivers/nagios:latest
```
alternatively you can use external Nagios configuration & log data with the following:

```shell
$ docker run --name nagios4  \
  -v /path-to-nagios/etc/:/opt/nagios/etc/ \
  -v /path-to-nagios/var:/opt/nagios/var/ \
  -v /path-to-custom-plugins:/opt/Custom-Nagios-Plugins \
  -v /path-to-nagiosgraph-var:/opt/nagiosgraph/var \
  -v /path-to-nagiosgraph-etc:/opt/nagiosgraph/etc \
  -p 0.0.0.0:8080:80 jasonrivers/nagios:latest
```
Note: The path for the custom plugins will be `/opt/Custom-Nagios-Plugins`, you will need to reference this directory in your configuration scripts.

The default credentials for the web interface is `nagiosadmin / nagios`

**Accessing Nagios Core Dashboard**

Lauch your Web browser and connect to <http://localhost:8080>. When asked for authentication, enter the default credentials. Th left sidebar provides links to the various functionnalities of Nagios.
The hosts links given an overview of the health of the minitored Hosts. In this case, the unique hosts is the Docker host.
![hots](images/hosts.png)

## Adding A Memory Usage service

The default installation of Nagios does not setup the monitoring RAM memory installation. The following capture shows that there is no RAM monitoring service available on the services dashboard. 

![noram](images/NoRAMService.png)

In order to install the RAM memory monitoring service, we should provision and configure the correspondat plugin. This plugin is already available as part of the plgins installed by NRPE (NRPE (Nagios Remote Plugin Executor).
>**NRPE** (Nagios Remote Plugin Executor) is used for executing Nagios plugins on remote client systems. This allows you to monitor remote machine metrics (disk usage, CPU load, etc.). 

NRPE can also communicate with some of the Windows agent addons, so you can execute scripts and check metrics on remote Windows machines as well.   

![nrpe](images/nrpe.png)

#### Steps to Configure the Memory usage service
- Connect to the Docker host and check that the RAM memory plugin (the PERL script `check_mem.pl`) is already existing in  `/opt/nagios/libexec` folder.

```shell
$ docker exec -it nagios4 /bin/bash
```
- Define a command in  `commands.cfg` which is located in `/opt/nagios/etc/objects`. The command is named `myCommand`. You may have to install `vim` text editor within the Docker host. To do so, use `apt-get update`  then `apt-get install vim`. 

```shell
define command{
        command_name     myCommand
        command_line   /opt/nagios/libexec/check_mem.pl -f -w 20 -c 10
 }
```
**-f** – For monitoring Free Memory
**-w 20** – Send a warning message if free memory is less 20% of the total memory.
**-c 10** – Send a critical message if free memory is less 10% of the total memory.

It is possible to test the command through the bash shell
```shell
root@a215221d6c26:/opt/nagios/libexec# ./check_mem.pl -f -w 20 -c 10
OK - 23.3% (597092 kB) free.|TOTAL=2560028KB;;;; USED=1962936KB;2048022;2304025;; FREE=597092KB;;;;
```
We see here that the free memory (23.3%) is higher the warning alert (20%).

- Define a service in  `localhost.cfg` which is located in `/opt/nagios/etc/objects`. The service is named `RAM` and uses the command `myCommand`. 
```shell
define service{
     use           local-service,graphed-service       ; Name of service template to use
     host_name     localhost
    service_description        RAM
    check_command              myCommand
}
```
- Restart nagios and varify that the RAM montinor has been already added to the services for localhost.
![ramadded](images/RAMServiceAdded.png)