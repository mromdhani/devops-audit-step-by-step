# Audit Spring Boot Applications on Kubernetes
---

## Steps

### Step 1 :
- Reuse the code of the Spring Boot application developed in Step  2
- Generate a Docker image and deploy it to DockerHub
- Install the application in Local Minikube using Helm
- Optional : Add custom metrics to the app like this one <https://github.com/alexandreroman/k8s-prometheus-micrometer-demo>

### Step 2
- If not installed install Prometheus Operator (that contains Grafana) in Local Minikube using Helm


### Step 3
- Monitor using Grafana some of the Actuator endpoints of the Spring Boot App.